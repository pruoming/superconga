**Summary**

(Summarize the bug here.)

**SuperConga version and Git branch**

(Paste the version of SuperConga here. It can be obtained from command line via `python superconga.py -V`. If possible, please also provide the git branch, obtained from command line via e.g. `git branch`. Please note that we support the current stable release of SuperConga, i.e. the main/master branch, as well as the development branch. If the bug appears in your own modified version of the code, or in an older version of SuperConga, then please first confirm that the bug can be reproduced in the current stable release.)

**Operating system**

(Write which operating system, distribution, and kernel version you are using.)

**GCC and CUDA**

(State which version of GCC and CUDA you are using. They can be obtained via `gcc --version` and `nvcc -V`. Please note that the latter should give the same version as `nvidia-smi`: if not, then the issue might be related to improper installation of NVIDIA drivers and CUDA. )

**Steps to reproduce**

(Please describe in detail the steps required to reproduce the bug, from setup and compilation to where the error occurred. If possible, please provide the `.json` config file and the exact command run, with all command-line arguments.)

**What is the current bug behavior?**

(Describe what happened: what is the actual error? If possible, please paste any error messages or output that might be relevant.)

**What did you expect the correct behavior to be?**

(What is the expected behavior.)

**Suggestions and possible fixes**

(If you have any thoughts on what the bug might be related to, or suggestions on how to fix it, then please feel free to share.)

/label ~Bug
