//===--------------- accelerators.h - All accelerators. -------------------===//
//
// Part of the SuperConga Project, under the GNU LGPL v3 license or higher.
// See LICENSE.txt for license information.
//
//===----------------------------------------------------------------------===//
///
/// \file
/// All accelerators.
///
//===----------------------------------------------------------------------===//
#ifndef CONGA_ACCELERATORS_H_
#define CONGA_ACCELERATORS_H_

#include "accelerators/Anderson.h"
#include "accelerators/BarzilaiBorwein.h"
#include "accelerators/CongAcc.h"
#include "accelerators/Picard.h"
#include "accelerators/Polyak.h"

#endif // CONGA_ACCELERATORS_H_
