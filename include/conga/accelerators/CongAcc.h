// Part of the SuperConga Project, under the GNU LGPL v3 license or higher.
// See LICENSE.txt for license information.
#ifndef CONGA_CONGACC_H_
#define CONGA_CONGACC_H_

#include "../Type.h"
#include "../configure.h"
#include "Accelerator.h"
#include "utils.h"

#if CONGA_HIP
#include <hip/hip_runtime.h>
#elif CONGA_CUDA
#include <cuda.h>
#endif

#include <json/json.h>
#include <thrust/device_vector.h>

#include <cassert>
#include <limits>
#include <vector>

namespace conga {
namespace accelerators {
template <typename T> class CongAcc : public Accelerator<T> {
public:
  /// \brief Constructor of the CongAcc accelerator.
  ///
  /// \param inStepSize The step size.
  /// \param inStepSizeFactor The factor used when adjusting the step size.
  /// \param inMinCosSimilarity The mininum tolerated cosine similarity.
  /// \param inMaxStepSize The maximum step size.
  /// \param inMinStepSize The minimum step size.
  explicit CongAcc(const T inStepSize = m_stepSizeDefault,
                   const T inStepSizeFactor = m_stepSizeFactorDefault,
                   const T inMinCosSimilarity = m_minCosSimilarityDefault,
                   const T inMaxStepSize = m_maxStepSizeDefault,
                   const T inMinStepSize = m_minStepSizeDefault);

  /// \brief Constructor of the CongAcc accelerator.
  ///
  /// \param inJSON JSON with parameters. Absent keys get default values.
  explicit CongAcc(const Json::Value &inJSON);

  /// \brief Destructor of the CongAcc accelerator.
  ~CongAcc(){};

  /// \brief Compute the next order parameter and vector potential.
  ///
  /// \param inOrderParameterG The right-hand side of the self-consistency
  /// equation for the order parameter, 'g(x)'.
  /// \param inVectorPotentialG The right-hand side of the self-consistency
  /// equation for the vector potential, 'g(x)'.
  /// \param inOutOrderParameterX The left-hand side of the self-consistency
  /// equation for the order parameter, 'x'.
  /// \param inOutVectorPotentialX The left-hand side of the self-consistency
  /// equation for the vector potential, 'x'.
  ///
  /// \return Void.
  void compute(const OrderParameter<T> &inOrderParameterG,
               const GridGPU<T> &inVectorPotentialG,
               OrderParameter<T> &inOutOrderParameterX,
               GridGPU<T> &inOutVectorPotentialX) override;

  /// \brief Compute the next vector potential.
  ///
  /// \param inVectorPotentialG The right-hand side of the self-consistency
  /// equation for the vector potential, 'g(x)'.
  /// \param inOutVectorPotentialX The left-hand side of the self-consistency
  /// equation for the vector potential, 'x'.
  ///
  /// \return Void.
  void compute(const GridGPU<T> &inVectorPotentialG,
               GridGPU<T> &inOutVectorPotentialX) override;

  /// \brief Compute the next order parameter.
  ///
  /// \param inOrderParameterG The right-hand side of the self-consistency
  /// equation for the order parameter, 'g(x)'.
  /// \param inOutOrderParameterX The left-hand side of the self-consistency
  /// equation for the order parameter, 'x'.
  ///
  /// \return Void.
  void compute(const OrderParameter<T> &inOrderParameterG,
               OrderParameter<T> &inOutOrderParameterX) override;

private:
  /// \brief Initialize the running mean, step sizes, and inertias.
  ///
  /// \param inX The left-hand side of the self-consistency equation. x.
  ///
  /// \return Void.
  void initialize(const GridGPU<T> &inX);

  /// \brief Compute the new inertial value using the bisection method.
  ///
  /// \param inDiffNorm The L2-norm of the difference.
  /// \param inMeanNorm The L2-norm of the running mean.
  /// \param inDotProduct The dot product between the difference and the running
  /// mean.
  /// \param inMinCosSimilarity The target minimum tolerated cosine similarity.
  ///
  /// \return The new inertia.
  T computeInertia(const T inDiffNorm, const T inMeanNorm, const T inDotProduct,
                   const T inMinCosSimilarity);

  /// \brief Compute the next order parameter and vector potential.
  ///
  /// \param inG The right-hand side of the self-consistency equation, 'g(x)'.
  /// \param inOutX The left-hand side of the self-consistency equation, 'x'.
  ///
  /// \return Void.
  void computeInternal(const GridGPU<T> &inG, GridGPU<T> &inOutX);

  // Defaults.
  static constexpr T m_stepSizeDefault = static_cast<T>(0.5);
  static constexpr T m_stepSizeFactorDefault = static_cast<T>(1.234);
  static constexpr T m_minCosSimilarityDefault = static_cast<T>(0.7);
  static constexpr T m_maxStepSizeDefault = static_cast<T>(100);
  static constexpr T m_minStepSizeDefault = static_cast<T>(0.001);

  // The initial step size.
  const T m_stepSizeInit;

  // Parameters used for adjusting the step size and inertia.
  const T m_stepSizeFactor;
  const T m_minCosSimilarity;

  // The limits of the step size.
  const T m_maxStepSize;
  const T m_minStepSize;

  // If initialized.
  bool m_initialized;

  // The running-mean.
  GridGPU<T> m_mean;

  // The step size per "field".
  std::vector<T> m_stepSize;

  // The inertia per "field".
  std::vector<T> m_inertia;
};

template <typename T>
CongAcc<T>::CongAcc(const T inStepSize, const T inStepSizeFactor,
                    const T inMinCosSimilarity, const T inMaxStepSize,
                    const T inMinStepSize)
    : Accelerator<T>(), m_stepSizeInit(inStepSize),
      m_stepSizeFactor(inStepSizeFactor),
      m_minCosSimilarity(inMinCosSimilarity), m_maxStepSize(inMaxStepSize),
      m_minStepSize(inMinStepSize), m_initialized(false) {
  assert(m_stepSizeInit > static_cast<T>(0));
  assert(m_stepSizeFactor > static_cast<T>(1));
  assert(m_minCosSimilarity >= static_cast<T>(0));
  assert(m_maxStepSize > static_cast<T>(0));
  assert(m_maxStepSize >= m_minStepSize);
  assert(m_minStepSize > static_cast<T>(0));
}

template <typename T>
CongAcc<T>::CongAcc(const Json::Value &inJSON)
    : Accelerator<T>(),
      m_stepSizeInit(inJSON.isMember("step_size")
                         ? static_cast<T>(inJSON["step_size"].asDouble())
                         : m_stepSizeDefault),
      m_stepSizeFactor(
          inJSON.isMember("step_size_factor")
              ? static_cast<T>(inJSON["step_size_factor"].asDouble())
              : m_stepSizeFactorDefault),
      m_minCosSimilarity(
          inJSON.isMember("cos_similarity_min")
              ? static_cast<T>(inJSON["cos_similarity_min"].asDouble())
              : m_minCosSimilarityDefault),
      m_maxStepSize(inJSON.isMember("step_size_max")
                        ? static_cast<T>(inJSON["step_size_max"].asDouble())
                        : m_maxStepSizeDefault),
      m_minStepSize(inJSON.isMember("step_size_min")
                        ? static_cast<T>(inJSON["step_size_min"].asDouble())
                        : m_minStepSizeDefault),
      m_initialized(false) {
  assert(m_stepSizeInit > static_cast<T>(0));
  assert(m_stepSizeFactor > static_cast<T>(1));
  assert(m_minCosSimilarity >= static_cast<T>(0));
  assert(m_minCosSimilarity <= static_cast<T>(1));
  assert(m_maxStepSize > static_cast<T>(0));
  assert(m_maxStepSize >= m_minStepSize);
  assert(m_minStepSize > static_cast<T>(0));
}

template <typename T> void CongAcc<T>::initialize(const GridGPU<T> &inX) {
  const int dimX = inX.dimX();
  const int dimY = inX.dimY();
  const int numFields = inX.numFields();
  const Type::type representation = inX.representation();

  m_mean = GridGPU<T>(dimX, dimY, numFields, representation);
  m_mean.setZero();

  m_stepSize.resize(numFields);
  std::fill(m_stepSize.begin(), m_stepSize.end(), m_stepSizeInit);

  m_inertia.resize(numFields);
  std::fill(m_inertia.begin(), m_inertia.end(), static_cast<T>(0));

  m_initialized = true;
}

template <typename T>
void CongAcc<T>::compute(const OrderParameter<T> &inOrderParameterG,
                         const GridGPU<T> &inVectorPotentialG,
                         OrderParameter<T> &inOutOrderParameterX,
                         GridGPU<T> &inOutVectorPotentialX) {
  // The names come from the self-consistency equation: x = g(x).
  const GridGPU<T> G = concatenate(inOrderParameterG, inVectorPotentialG);
  GridGPU<T> X = concatenate(inOutOrderParameterX, inOutVectorPotentialX);
  computeInternal(G, X);
  split(X, inOutOrderParameterX, inOutVectorPotentialX);
}

template <typename T>
void CongAcc<T>::compute(const GridGPU<T> &inVectorPotentialG,
                         GridGPU<T> &inOutVectorPotentialX) {
  computeInternal(inVectorPotentialG, inOutVectorPotentialX);
}

template <typename T>
void CongAcc<T>::compute(const OrderParameter<T> &inOrderParameterG,
                         OrderParameter<T> &inOutOrderParameterX) {
  // The names come from the self-consistency equation: x = g(x).
  const GridGPU<T> G = concatenate(inOrderParameterG);
  GridGPU<T> X = concatenate(inOutOrderParameterX);
  computeInternal(G, X);
  split(X, inOutOrderParameterX);
}

namespace adaptive_ball_internal {
template <typename T>
__global__ void computeInternal(const int inDimX, const int inDimY,
                                const int inDimZ, const T *const inStepSize,
                                const T *const inInertia, const T *const inDiff,
                                T *const inOutMean, T *const inOutX) {
  // Indices of spatial coordinates.
  const int x = blockIdx.x * blockDim.x + threadIdx.x;
  const int y = blockIdx.y * blockDim.y + threadIdx.y;

  // Field.
  const int z = blockIdx.z * blockDim.z + threadIdx.z;

  if (x < inDimX && y < inDimY && z < inDimZ) {
    // Linear index.
    const int idx = inDimX * (z * inDimY + y) + x;

    // Update mean.
    const T inertia = inInertia[z];
    const T mean =
        inOutMean[idx] * inertia + inDiff[idx] * (static_cast<T>(1) - inertia);
    inOutMean[idx] = mean;

    // Compute the result.
    inOutX[idx] += inStepSize[z] * mean;
  }
}
} // namespace adaptive_ball_internal

template <typename T>
T CongAcc<T>::computeInertia(const T inDiffNorm, const T inMeanNorm,
                             const T inDotProduct, const T inMinCosSimilarity) {
  // In the following x = inertia.

  // Do everything in double precision.
  const double diffNorm = static_cast<double>(inDiffNorm);
  const double meanNorm = static_cast<double>(inMeanNorm);
  const double dotProduct = static_cast<double>(inDotProduct);
  const double minCosSimilarity = static_cast<double>(inMinCosSimilarity);

  // The error when x = 1.
  double errorA = dotProduct / (diffNorm * meanNorm) - minCosSimilarity;
  double xA = 1.0;

  // The error when x = 0.
  double errorB = 1.0 - minCosSimilarity;
  double xB = 0.0;

  // Initial guess, in the middle.
  double x = 0.5 * (xA + xB);

  // We don't have to be very strict.
  const double convergenceCriterion = 0.01;

  // Loop until converged.
  bool isConverged = false;
  const int numIterations = 10;
  for (int i = 0; i < numIterations; ++i) {
    // Compute the similarity.
    const double y = (1.0 - x);
    const double numer = x * dotProduct + y * diffNorm * diffNorm;
    const double denom = diffNorm * std::sqrt(x * x * meanNorm * meanNorm +
                                              2.0 * x * y * dotProduct +
                                              y * y * diffNorm * diffNorm);
    const double similarity = numer / denom;

    // Check convergence.
    const double error = similarity - minCosSimilarity;
    isConverged = std::abs(error) < convergenceCriterion;
    if (isConverged) {
      break;
    }

    // Replace an end point.
    if (utils::sign(error) == utils::sign(errorA)) {
      errorA = error;
      xA = x;
    } else {
      errorB = error;
      xB = x;
    }

    // Make sure that the signs of errorA and errorB differ.
    if (utils::sign(errorA) == utils::sign(errorB)) {
      // Something weird has happened.
      break;
    }

    // New guess.
    x = 0.5 * (xA + xB);
  }

#ifndef NDEBUG
  if (!isConverged) {
    std::cerr << "-- WARNING! The computation of the inertia did not converge."
              << std::endl;
  }
#endif

  return static_cast<T>(x);
}

template <typename T>
void CongAcc<T>::computeInternal(const GridGPU<T> &inG, GridGPU<T> &inOutX) {
  // Initialize grids.
  if (!m_initialized) {
    initialize(inOutX);
  }

  // Dimensions.
  const int dimX = inOutX.dimX();
  const int dimY = inOutX.dimY();
  const int numFields = inOutX.numFields();

  // The difference.
  const GridGPU<T> diff = inG - inOutX;

  // Use for changing the step size and inertia.
  const GridGPU<T> D2 = diff * diff;
  const GridGPU<T> M2 = m_mean * m_mean;
  const GridGPU<T> DM = diff * m_mean;

  for (int i = 0; i < numFields; ++i) {
    const T d = std::sqrt(D2.sumField(i));
    const T m = std::sqrt(M2.sumField(i));
    const T denom = d * m;

    if (denom > static_cast<T>(0)) {
      // Compute the cosine ditsance.
      const T dot = DM.sumField(i);
      const T cosSimilarity = dot / denom;

      // Adjust the the step size. Increase step size if similar, otherwise
      // decrease it.
      m_stepSize[i] *= std::pow(m_stepSizeFactor, cosSimilarity);

      // New inertia.
      if (cosSimilarity < m_minCosSimilarity) {
        m_inertia[i] = computeInertia(d, m, dot, m_minCosSimilarity);
      }
    }

    // Clamp to range.
    m_stepSize[i] = std::clamp(m_stepSize[i], m_minStepSize, m_maxStepSize);
  }

  // Copy to the GPU.
  const thrust::device_vector<T> stepSizeGPU(m_stepSize);
  const thrust::device_vector<T> inertiaGPU(m_inertia);

  // Kernel parameters.
  const auto [numBlocks, numThreadsPerBlock] =
      inOutX.getKernelDimensionsFields();

  // Take a step.
  adaptive_ball_internal::computeInternal<<<numBlocks, numThreadsPerBlock>>>(
      dimX, dimY, numFields, thrust::raw_pointer_cast(stepSizeGPU.data()),
      thrust::raw_pointer_cast(inertiaGPU.data()), diff.getDataPointer(),
      m_mean.getDataPointer(), inOutX.getDataPointer());
}
} // namespace accelerators
} // namespace conga

#endif // CONGA_CONGACC_H_
